FROM linux:latest

ARG USER=trinadha
ARG S2IDIR="/home/s2i"
ARG APPDIR="/deployments"

LABEL maintainer="Trinadha <trinadha.mouli@capgemini.com>" \
      io.k8s.description="S2I builder for Java Applications." \
      io.k8s.display-name="test Environment" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,java,maven,gradle" \
      io.openshift.s2i.scripts-url="image://$S2IDIR/bin"

COPY s2i $S2IDIR
RUN chmod 777 -R $S2IDIR

COPY jdkinstaller.sh "$APPDIR/"


RUN ["/bin/bash", "-c", "$APPDIR/jdkinstaller.sh"]

RUN sudo yum install maven
    
RUN rm -rf /var/lib/apt/lists/*

WORKDIR $APPDIR

EXPOSE 8080

USER $USER

CMD ["$S2IDIR/bin/run"]
